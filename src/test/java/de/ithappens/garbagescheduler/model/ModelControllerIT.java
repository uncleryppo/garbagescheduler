package de.ithappens.garbagescheduler.model;

import de.ithappens.garbagescheduler.ResourceBundleDictionary;
import java.awt.Color;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import lombok.extern.log4j.Log4j2;
import org.junit.After;
import org.junit.Assert;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author christianrybotycky
 */
@Log4j2
public class ModelControllerIT {
    
    private ModelController instance;
    private final String vaultLocation = "testSaveCurrentVault.gs";

    
    public ModelControllerIT() {
    }
    
    @Before
    public void setUp() {
        instance = new ModelController();
    }
    
    @After
    public void tearDown() {
        Path path = FileSystems.getDefault().getPath(".", vaultLocation);
        try {
            Files.deleteIfExists(path);
        } catch (IOException ex) {
            log.error(ex);
        }
    }

    /**
     * Test of RB method, of class ModelController.
     */
    @Test
    public void testRB() {
        log.debug("RB");
        ResourceBundleDictionary result = instance.RB();
        Assert.assertNotNull(result);
    }

    /**
     * Test of CSS method, of class ModelController.
     */
    @Test
    public void testCSS() {
        log.debug("CSS");
        String result = instance.CSS();
        Assert.assertNotNull(result);
    }

    /**
     * Test of getCurrentVault method, of class ModelController.
     */
    @Test
    public void testGetCurrentVault() {
        log.debug("getCurrentVault");
        Vault result = instance.getCurrentVault();
        Assert.assertNotNull(result);
    }

    /**
     * Test of loadVault method, of class ModelController.
     */
    @Test
    public void testLoadVault() {
        log.debug("loadVault");
        String vaultLocation = "";
        boolean expResult_thrownException = true;
        boolean result_thrownException = false;
        try {
            Vault vault = instance.loadVault(vaultLocation, false);
        } catch (IOException ex) {
            result_thrownException = true;
        }
        assertEquals(expResult_thrownException, result_thrownException);
    }

    /**
     * Test of saveCurrentVault method, of class ModelController.
     * @throws java.lang.Exception
     */
    @Test
    public void testSaveCurrentVault() throws Exception {
        log.debug("saveCurrentVault");
        instance.saveCurrentVault(vaultLocation);
    }
    
    /**
     * Test of saveCurrentVault method, of class ModelController.
     * Having data
     * @throws java.lang.Exception
     */
    @Test
    public void testSaveCurrentVault_withData() throws Exception {
        log.debug("saveCurrentVault");
        prepareTestDataInCurrentVault();
        int savedDustBins = instance.getCurrentVault().getAvailableDustBins() != null ? instance.getCurrentVault().getAvailableDustBins().size() : 0;
        if (savedDustBins == 0) {
            Assert.fail("Not enough test data there...");
        }
        instance.saveCurrentVault(vaultLocation + ".json");
        instance.getCurrentVault().setAvailableDustBins(null);
        instance.loadVault(vaultLocation + ".json", false);
        int loadedDustBins = instance.getCurrentVault().getAvailableDustBins() != null ? instance.getCurrentVault().getAvailableDustBins().size() : 0;
        assertEquals(savedDustBins, loadedDustBins);
    }
    
    private void prepareTestDataInCurrentVault() {
        DustBin dustBin1 = new DustBin();
        dustBin1.setColor(Color.CYAN.toString());
        dustBin1.setUniqueId("DustBin1");
        dustBin1.setSizeInLiter(150);
        dustBin1.setScopeGarbage("In-scope: bio");
        dustBin1.setForbiddenGarbage("Forbidden: chemical");
        List<DustBin> dustBins = new ArrayList<>();
        dustBins.add(dustBin1);
        
        ClearingEvent clearingEvent1 = new ClearingEvent();
        clearingEvent1.setClearedDustBins(new DustBin[]{dustBin1});
        clearingEvent1.setDateOfPlannedClearing(Calendar.getInstance());
        Calendar executed = Calendar.getInstance();
        executed.add(Calendar.DAY_OF_MONTH, 1);
        clearingEvent1.setDateOfExecutedClearing(executed);
        List<ClearingEvent> clearingEvents = new ArrayList<>();
        clearingEvents.add(clearingEvent1);
        
        Vault currentVault = instance.getCurrentVault();
        currentVault.setAvailableDustBins(dustBins);
        currentVault.setClearingEvents(clearingEvents);
    }
   
}
