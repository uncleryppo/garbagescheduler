package de.ithappens.garbagescheduler;

import java.util.prefs.Preferences;
import org.apache.commons.lang3.StringUtils;

/**
 * Copyright: 2016 - 2017
 * Organisation: IT-Happens.de
 * @author Christian.Rybotycky
 */
public class UserPreferences {

    private final String KEY__LOCATION_OF_LAST_USED_VAULT_FILE = "location-of-last-used-vault-file";
    private final String KEY__SUFFIX_FOR_BACKUP_FILE = "suffix-for-backup-file";
    public final String DEFAULT__SUFFIX_FOR_BACKUP_FILE = "-backup-";
    private final String KEY__ENABLE_BACKUP = "enable-backup";

    private final Preferences prefs;

    public UserPreferences() {
        prefs = Preferences.userNodeForPackage(getClass());
    }

    public void setLocationOfLastUsedVaultFile(String locationOfLastUsedVaultFile) {
        prefs.put(KEY__LOCATION_OF_LAST_USED_VAULT_FILE, locationOfLastUsedVaultFile);
    }

    public String getLocationOfLastUsedVaultFile() {
        return prefs.get(KEY__LOCATION_OF_LAST_USED_VAULT_FILE, "");
    }

    public void setSuffixForBackupFile(String suffixForBackupFile) {
        if (StringUtils.isEmpty(suffixForBackupFile)) {
            suffixForBackupFile = DEFAULT__SUFFIX_FOR_BACKUP_FILE;
        }
        prefs.put(KEY__SUFFIX_FOR_BACKUP_FILE, suffixForBackupFile);
    }

    public String getSuffixForBackupFile() {
        return prefs.get(KEY__SUFFIX_FOR_BACKUP_FILE, DEFAULT__SUFFIX_FOR_BACKUP_FILE);
    }
    
    public boolean getEnableBackup() {
        return prefs.getBoolean(KEY__ENABLE_BACKUP, false);
    }
    
    public void setEnableBackup(boolean disableBackup) {
        prefs.putBoolean(KEY__ENABLE_BACKUP, disableBackup);
    }

}
