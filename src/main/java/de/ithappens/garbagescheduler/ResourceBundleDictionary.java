package de.ithappens.garbagescheduler;

import java.util.MissingResourceException;
import java.util.ResourceBundle;
import lombok.extern.log4j.Log4j2;

/**
 * Copyright: 2016 - 2017
 * Organisation: IT-Happens.de
 * @author Christian.Rybotycky
 */
@Log4j2
public class ResourceBundleDictionary {

    public final ResourceBundle RB = ResourceBundle.getBundle("de.ithappens.garbage-scheduler");

    public String ABOUT = get("ABOUT");
    public String APPLICATION = get("APPLICATION");
    public String APPLICATION_NAME = get("APPLICATION_NAME");
    public String APPLICATION_VERSION = get("APPLICATION_VERSION");
    public String DEVELOPED_BY = get("DEVELOPED_BY");
    public String FILE_VERSION = get("FILE_VERSION");
    public String FULLSCREEN = get("FULLSCREEN");
    public String OK = get("OK");
    public String PROVIDED_BY = get("PROVIDED_BY");
    public String VERSION = get("VERSION");
    public String QUIT = get("QUIT");
    public String OS_ARCHITECTURE = get("OS_ARCHITECTURE");
    public String OS_NAME = get("OS_NAME");
    public String OS_VERSION = get("OS_VERSION");
    public String OS_USER_DIR = get("OS_USER_DIR");
    public String OS_USER_HOME = get("OS_USER_HOME");
    public String JAVA_VERSION = get("JAVA_VERSION");
    public String JAVA_LOCATION = get("JAVA_LOCATION");
    public String JAVA_VENDOR = get("JAVA_VENDOR");
    public String JAVA_VENDOR_URL = get("JAVA_VENDOR_URL");
    //functions
    public String LOAD = get("LOAD");
    public String SAVE = get("SAVE");
    public String SAVE_DONE = get("SAVE_DONE");
    public String UPDATE = get("UPDATE");
    public String ADD = get("ADD");
    public String DELETE = get("DELETE");
    //storage: vault
    public String VAULT_LOCATION = get("VAULT_LOCATION");
    public String VAULT = get("VAULT");
    public String NEW_VAULT = get("NEW_VAULT");
    public String CREATION_DATE = get("CREATION_DATE");
    public String CREATION_USER = get("CREATION_USER");
    public String CREATION_SYSTEM = get("CREATION_SYSTEM");
    public String CHANGE_DATE = get("CHANGE_DATE");
    public String CHANGE_USER = get("CHANGE_USER");
    public String CHANGE_SYSTEM = get("CHANGE_SYSTEM");
    public String VAULT_BACKUP_SUCCESSFUL = get("VAULT_BACKUP_SUCCESSFUL");
    public String VAULT_BACKUP_NOT_SUCCESSFUL = get("VAULT_BACKUP_NOT_SUCCESSFUL");
    public String ENABLE_BACKUP = get("ENABLE_BACKUP");

    public String DUSTBIN = get("DUSTBIN");
    public String DUSTBINS = get("DUSTBINS");
    public String UNIQUE_ID = get("UNIQUE_ID");
    public String SIZE_IN_LITER = get("SIZE_IN_LITER");
    public String COLOR = get("COLOR");
    public String SCOPE_GARBAGE = get("SCOPE_GARBAGE");
    public String FORBIDDEN_GARBAGE = get("FORBIDDEN_GARBAGE");
    public String ADD_DUSTBIN = get("ADD_DUSTBIN");
    public String DELETE_DUSTBIN = get("DELETE_DUSTBIN");
    public String NEW_DUSTBIN = get("NEW_DUSTBIN");

    public String CLEARING_EVENTS = get("CLEARING_EVENTS");
    public String PLANNED_DATE_OF_CLEARING = get("PLANNED_DATE_OF_CLEARING");
    public String DATE_OF_EXECUTED_CLEARING = get("DATE_OF_EXECUTED_CLEARING");
    public String CLEARED_DUSTBINS = get("CLEARED_DUSTBINS");
    public String PLANNED_DUSTBINS_TO_CLEAR = get("PLANNED_DUSTBINS_TO_CLEAR");
    public String ADD_CLEARING_EVENT = get("ADD_CLEARING_EVENT");
    public String DELETE_CLEARING_EVENT = get("DELETE_CLEARING_EVENT");

    public String CLEARING_CONTRACTS = get("CLEARING_CONTRACTS");
    public String VALID_FROM = get("VALID_FROM");
    public String VALID_TO = get("VALID_TO");
    public String COUNT_INCLUDED_FREE_CLEARINGS = get("COUNT_INCLUDED_FREE_CLEARINGS");
    public String PRICE_PER_CLEARING = get("PRICE_PER_CLEARING");
    public String PRICE_PER_KILOGRAM = get("PRICE_PER_KILOGRAM");
    public String ADD_CLEARING_CONTRACT = get("ADD_CLEARING_CONTRACT");
    public String DELETE_CLEARING_CONTRACT = get("DELETE_CLEARING_CONTRACT");
    public String NEW_CLEARING_CONTRACT = get("NEW_CLEARING_CONTRACT");

    public String get(String key) {
        return get(RB, key);
    }

    public String get(ResourceBundle bundle, String key) {
        String value = null;
        try {
            value = bundle.getString(key);
        } catch (MissingResourceException mre) {
            log.warn("Requested resource '" + key + "' missing in bundle of locale '" + bundle.getLocale().toString() + "'.", mre);
        }
        return value != null ? value : key;
    }

}
