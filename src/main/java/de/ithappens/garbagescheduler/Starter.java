package de.ithappens.garbagescheduler;

/**
 * Copyright: 2020
 * Organization: IT-Happens.de
 *
 * @author Ryppo
 */
public class Starter {

    public static void main(String args[]) {
        GarbageScheduler.main(args);
    }

}
