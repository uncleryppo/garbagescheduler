package de.ithappens.garbagescheduler.view;

import java.net.URL;
import javafx.scene.image.Image;
import javax.swing.ImageIcon;

/** 
 * Copyright: 2016 - 2017
 * Organisation: IT-Happens.de
 * @author Christian.Rybotycky
*/
public class IconDictionary {
    
    public static final String BASE_PATH = "de/ithappens/";
    public static final String RESOURCE_PATH = BASE_PATH + "garbagescheduler/";
    public static final String IMAGE_PATH = RESOURCE_PATH + "img/";

    public static final Image APPLICATION_ICON = getFxImage("086__empty.png");
    public static final ImageIcon APPLICATION__IMAGE_ICON = getImageIcon("trash-512.png");
    public static final Image FULLSCREEN_ICON = getFxImage("096__zoom in.png");
    public static final Image INFO_ICON = getFxImage("043__hint.png");
    public static final Image QUIT_ICON = getFxImage("057__power.png");
    public static String MISSING_ICON = "picture-empty.png";
    
    public static final Image LOAD_ICON = getFxImage("025__login.png");
    public static final Image SAVE_ICON = getFxImage("026__logout.png");
    public static final Image NEW_VAULT_ICON = getFxImage("037__file_plus.png");

    public static Image getFxImage(String filename) {
        return new Image(IconDictionary.class.getClassLoader().getResourceAsStream(IMAGE_PATH + filename));
    }
    
    private static ImageIcon getImageIcon(String iconName) {
        URL resourceURL = IconDictionary.class.getClassLoader().getResource(IMAGE_PATH + iconName);
        if (resourceURL == null) {
            resourceURL = IconDictionary.class.getClassLoader().getResource(IMAGE_PATH + MISSING_ICON);
        }
        return new ImageIcon(resourceURL);
    }

}
