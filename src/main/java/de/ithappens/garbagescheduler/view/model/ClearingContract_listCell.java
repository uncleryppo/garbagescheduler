package de.ithappens.garbagescheduler.view.model;

import de.ithappens.garbagescheduler.model.ClearingContract;
import java.text.SimpleDateFormat;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;

/**
 * Copyright: 2016 - 2017
 * Organisation: IT-Happens.de
 * @author Christian.Rybotycky
 */
@Log4j2
public class ClearingContract_listCell extends IModel_listCell<ClearingContract>{
    
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy");

    @Override
    public String getItemText(ClearingContract item) {
        String text = null;
        if (item != null) {
            text = StringUtils.defaultString(I_node.convertCalendar(sdf, item.getValidFrom()));
            if (item.getDustBin() != null) {
                text += " - " + item.getDustBin().getColor();
            }
        }
        return text;
    }

}
