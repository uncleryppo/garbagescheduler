package de.ithappens.garbagescheduler.view.model;

import de.ithappens.garbagescheduler.model.ClearingEvent;
import lombok.extern.log4j.Log4j2;

/**
 * Copyright: 2016 - 2017
 * Organisation: IT-Happens.de
 * @author Christian.Rybotycky
 */
@Log4j2
public class ClearingEvent_listCell extends IModel_listCell<ClearingEvent> {
    
    @Override
    public String getItemText(ClearingEvent item) {
        return I_node.convertToDate(item.getDate());
    }
    
}
