package de.ithappens.garbagescheduler.view.model;

import de.ithappens.garbagescheduler.model.DustBin;
import lombok.extern.log4j.Log4j2;

/**
 * Copyright: 2016 - 2017
 * Organisation: IT-Happens.de
 * @author Christian.Rybotycky
 */
@Log4j2
public class DustBin_listCell extends IModel_listCell<DustBin>{

    @Override
    public String getItemText(DustBin item) {
        return item.getUniqueId();
    }
    
}
