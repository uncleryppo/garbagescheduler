package de.ithappens.garbagescheduler.view.model;

import de.ithappens.garbagescheduler.model.I_model;
import de.ithappens.garbagescheduler.model.ModelController;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.layout.FlowPane;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

/**
 * Copyright: 2016 - 2017
 * Organisation: IT-Happens.de
 * @author Christian.Rybotycky
 * @param <E>
 */
@Log4j2
public abstract class IModel_node<E extends I_model> extends I_node {

    private FlowPane buttonBar;
    @Getter
    @Setter
    private E currentModel;
    private ListView<E> model_liVw;

    public IModel_node(ModelController mc) {
        super(mc);
    }

    protected ListView<E> getModelListView() {
        if (model_liVw == null) {
            model_liVw = new ListView<>();
            model_liVw.setCellFactory((ListView<E> livw) -> {
                return getModelCellFactory();
            });
            model_liVw.getSelectionModel().selectedItemProperty().addListener(
                    (ObservableValue<? extends E> ov, E t, E t1)
                    -> {
                currentModel = t1;
                bindToCurrentModel();
            });
        }
        return model_liVw;
    }

    protected boolean currentModelExists() {
        return getCurrentModel() != null;
    }

    protected abstract void bindToCurrentModel();

    protected abstract IModel_listCell<E> getModelCellFactory();

    protected FlowPane getButtonBar() {
        if (buttonBar == null) {
            Button updateView_btn = new Button(MC().RB().UPDATE);
            updateView_btn.setOnAction((ActionEvent e) -> {
                bindUi();
            });
            Button add_btn = new Button(MC().RB().ADD);
            add_btn.setOnAction((ActionEvent e) -> {
                actionAdd();
            });
            Button delete_btn = new Button(MC().RB().DELETE);
            delete_btn.setOnAction((ActionEvent e) -> {
                actionDelete();
            });
            Button saveVault_btn = new Button(MC().RB().SAVE);
            saveVault_btn.setOnAction((ActionEvent e) -> {
                Task<Void> task = new Task<Void>() {
                    @Override
                    protected Void call() throws Exception {
                        saveVault_btn.setDisable(true);
                        actionSaveVault();
                        updateMessage(MC().RB().SAVE_DONE + "...");
                        int ival = 250;
                        Thread.sleep(ival);
                        updateMessage(MC().RB().SAVE_DONE + "..");
                        Thread.sleep(ival);
                        updateMessage(MC().RB().SAVE_DONE + ".");
                        Thread.sleep(ival);
                        updateMessage(MC().RB().SAVE_DONE);
                        Thread.sleep(ival);
                        updateMessage(MC().RB().SAVE);
                        saveVault_btn.setDisable(false);
                        return null;
                    }
                };
                task.messageProperty().addListener((observable, oldValue, newValue) -> {
                    saveVault_btn.setText(newValue);
                });
                new Thread(task).start();
            });
            buttonBar = buildButtonBar(updateView_btn, add_btn, delete_btn, saveVault_btn);
        }
        return buttonBar;
    }

    @Override
    public void bindUi() {
        log.debug("bindUi");
        ObservableList<E> modelsList = FXCollections.observableArrayList();
        if (vaultExists()) {
            List<E> models = getModelsFromCurrentVault();
            if (models != null) {
                for (E model : models) {
                    modelsList.add(model);
                }
            }
        }
        FXCollections.sort(modelsList);
        getModelListView().setItems(modelsList);
        getModelListView().refresh();
    }

    protected abstract List<E> getModelsFromCurrentVault();

    protected abstract void setModelsToCurrentVault(List<E> models);

    private void actionAdd() {
        if (vaultExists()) {
            E createdModel = createModel();
            setCurrentModel(createdModel);
            List<E> models = getModelsFromCurrentVault();
            if (models == null) {
                models = new ArrayList<>();
                setModelsToCurrentVault(models);
            }
            models.add(createdModel);
            bindUi();
            getModelListView().getSelectionModel().select(createdModel);
        }
    }

    protected abstract E createModel();

    protected void actionDelete() {
        if (vaultExists() && getModelsFromCurrentVault() != null) {
            List<E> models = getModelsFromCurrentVault();
            models.remove(getCurrentModel());
            setCurrentModel(null);
            bindUi();
        }
    }

    private void actionSaveVault() {
        actionBeforeSaveVault();
        E currentSelectedModel = getCurrentModel();
        getModelListView().getSelectionModel().select(null);
        try {
            MC().saveCurrentVault();
        } catch (IOException ex) {
            log.error(ex);
        }
        bindUi();
        actionAfterSaveVault();
        getModelListView().getSelectionModel().select(currentSelectedModel);

    }

    protected abstract void actionBeforeSaveVault();

    protected abstract void actionAfterSaveVault();

    protected FlowPane buildButtonBar(Button... buttons) {
        FlowPane buttonPane = new FlowPane(buttons);
        buttonPane.setVgap(8);
        buttonPane.setHgap(4);
        return buttonPane;
    }
}
