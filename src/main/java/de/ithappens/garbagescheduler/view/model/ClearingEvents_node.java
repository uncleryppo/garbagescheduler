package de.ithappens.garbagescheduler.view.model;

import de.ithappens.garbagescheduler.model.ClearingEvent;
import de.ithappens.garbagescheduler.model.DustBin;
import de.ithappens.garbagescheduler.model.ModelController;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.FlowPane;
import lombok.extern.log4j.Log4j2;
import org.controlsfx.control.CheckListView;

/**
 * Copyright: 2016 - 2017
 * Organisation: IT-Happens.de
 * @author Christian.Rybotycky
 */
@Log4j2
public class ClearingEvents_node extends IModel_node<ClearingEvent> {
    
    private DatePicker dateOfPlanningClearing_dp, dateOfExecutedClearing_dp;
    private CheckListView<DustBin> clearedDustBins_clv, plannedDustBinsToClear_clv;
    private final int clv_prefWidth = 150;

    public ClearingEvents_node(ModelController mc) {
        super(mc);
    }

    @Override
    protected void buildUi() {
        Label plannedDateOfClearing_lbl = new Label(MC().RB().PLANNED_DATE_OF_CLEARING);
        dateOfPlanningClearing_dp = new DatePicker();
        dateOfPlanningClearing_dp.setOnAction((ActionEvent t) -> {
            if (currentModelExists()) {
                getCurrentModel().setDateOfPlannedClearing(convert(dateOfPlanningClearing_dp.getValue()));
            }
            getModelListView().refresh();
        });
        Label plannedDustBinsToClear_lbl = new Label(MC().RB().PLANNED_DUSTBINS_TO_CLEAR);
        plannedDustBinsToClear_clv = new  CheckListView<>();
        plannedDustBinsToClear_clv.setPrefHeight(clv_prefWidth);
        
        Label dateOfExecutedClearing_lbl = new Label(MC().RB().DATE_OF_EXECUTED_CLEARING);
        dateOfExecutedClearing_dp = new DatePicker();
        dateOfExecutedClearing_dp.setOnAction((ActionEvent t) -> {
            if (currentModelExists()) {
                getCurrentModel().setDateOfExecutedClearing(convert(dateOfExecutedClearing_dp.getValue()));
            }
            getModelListView().refresh();
        });
        Label clearedDustBins_lbl = new Label(MC().RB().CLEARED_DUSTBINS);
        clearedDustBins_clv = new CheckListView<>();
        clearedDustBins_clv.setPrefHeight(clv_prefWidth);
        //layout
        ColumnConstraints column1 = new ColumnConstraints();
        column1.setPercentWidth(30);
        ColumnConstraints column2 = new ColumnConstraints();
        column2.setPercentWidth(30);
        ColumnConstraints column3 = new ColumnConstraints();
        column3.setPercentWidth(35);
        getColumnConstraints().addAll(column1, column2, column3);
        int row = 0;
        int rowspan = 1;
        int colspan = 1;
        
        FlowPane buttonBar = getButtonBar();
        add(buttonBar, 0, row, 4, rowspan);
        row++;
        add(getModelListView(), 0, row, colspan, 4);
        add(plannedDateOfClearing_lbl, 1, row);
        add(dateOfPlanningClearing_dp, 2, row);
        row++;
        add(plannedDustBinsToClear_lbl, 1, row);
        int lbvRowSpan = 1;
        int lbvColSpan = 1;
        add(plannedDustBinsToClear_clv, 2, row, lbvColSpan, lbvRowSpan);
        row = row + lbvRowSpan;
        add(dateOfExecutedClearing_lbl, 1, row);
        add(dateOfExecutedClearing_dp, 2, row);
        row++;
        add(clearedDustBins_lbl, 1, row);
        add(clearedDustBins_clv, 2, row, lbvColSpan, lbvRowSpan);
        row = row + lbvRowSpan;
        add(new Separator(), 1, row, getColumnConstraints().size() - 1, rowspan);
    }

    @Override
    public String getTitle() {
        return MC().RB().CLEARING_EVENTS;
    }
    
    private DustBin[] convert(ObservableList<DustBin> sourceDustBins) {
        DustBin[] targetDustBins = null;
        if (sourceDustBins != null) {
            targetDustBins = new DustBin[sourceDustBins.size()];
            for (int i = 0; i < sourceDustBins.size(); i++) {
                targetDustBins[i] = sourceDustBins.get(i);
            }
        }
        return targetDustBins;
    }
    
    private void bindCheckListView(CheckListView<DustBin> checkListView, DustBin[] dustBinsToCheck) {
        checkListView.getCheckModel().clearChecks();
        if (dustBinsToCheck != null) {
            ObservableList<DustBin> availableDustBins = checkListView.getItems();
            if (availableDustBins != null) {
                for (DustBin dustBinToCheck : dustBinsToCheck) {
                    boolean itemFound = false;
                    for (DustBin availableDustBin : availableDustBins) {
                        if (dustBinToCheck.getUniqueId().equals(availableDustBin.getUniqueId())) {
                            checkListView.getCheckModel().check(availableDustBin);
                            log.debug(dustBinToCheck.getUniqueId());
                            itemFound = true;
                            break;
                        }
                    }
                    if (!itemFound) {
                        availableDustBins.add(dustBinToCheck);
                        checkListView.getCheckModel().check(dustBinToCheck);
                    }
                }
            }
        }
    }

    @Override
    protected void bindToCurrentModel() {
        //reset the UI
        ObservableList<DustBin> availableDustBinsListForPlanning = FXCollections.observableArrayList();
        ObservableList<DustBin> availableDustBinsListForExecution = FXCollections.observableArrayList();
        if (MC().getCurrentVault() != null && MC().getCurrentVault().getAvailableDustBins() != null) {
            for (DustBin availableDustBin : MC().getCurrentVault().getAvailableDustBins()) {
                availableDustBinsListForPlanning.add(new DustBin(availableDustBin));
                availableDustBinsListForExecution.add(new DustBin(availableDustBin));
            }
        }
        plannedDustBinsToClear_clv.setItems(availableDustBinsListForPlanning);
        plannedDustBinsToClear_clv.getCheckModel().clearChecks();
        clearedDustBins_clv.setItems(availableDustBinsListForExecution);
        clearedDustBins_clv.getCheckModel().clearChecks();
        //bind the UI
        if (currentModelExists()) {
            dateOfPlanningClearing_dp.setValue(convert(getCurrentModel().getDateOfPlannedClearing()));
            DustBin[] plannedDustBinsToClear = getCurrentModel().getPlannedDustBinsToClear();
            bindCheckListView(plannedDustBinsToClear_clv, plannedDustBinsToClear);

            dateOfExecutedClearing_dp.setValue(convert(getCurrentModel().getDateOfExecutedClearing()));
            DustBin[] clearedDustBins = getCurrentModel().getClearedDustBins();
            bindCheckListView(clearedDustBins_clv, clearedDustBins);
        } else {
            dateOfPlanningClearing_dp.setValue(null);
            dateOfExecutedClearing_dp.setValue(null);
        }
    }

    @Override
    protected IModel_listCell<ClearingEvent> getModelCellFactory() {
        return new ClearingEvent_listCell();
    }

    @Override
    protected List<ClearingEvent> getModelsFromCurrentVault() {
        return vaultExists() ? getCurrentVault().getClearingEvents() : null;
    }

    @Override
    protected void setModelsToCurrentVault(List<ClearingEvent> models) {
        if (vaultExists()) {
            getCurrentVault().setClearingEvents(models);
        }
    }

    @Override
    protected ClearingEvent createModel() {
        return new ClearingEvent();
    }

    @Override
    protected void actionBeforeSaveVault() {
        if (getCurrentModel() != null) {
            getCurrentModel().setPlannedDustBinsToClear(convert(plannedDustBinsToClear_clv.getCheckModel().getCheckedItems()));
            getCurrentModel().setClearedDustBins(convert(clearedDustBins_clv.getCheckModel().getCheckedItems()));
        }
    }

    @Override
    protected void actionAfterSaveVault() {
    }

}
