package de.ithappens.garbagescheduler.view.model;

import javafx.scene.control.Label;
import javafx.scene.control.ListCell;

/**
 * Copyright: 2016 - 2017
 * Organisation: IT-Happens.de
 * @author Christian.Rybotycky
 * @param <T>
 */
public abstract class IModel_listCell<T> extends ListCell<T> {

    @Override
    public void updateItem(T item, boolean empty) {
        super.updateItem(item, empty);
        Label name_lbl = new Label();
        if (item != null) {
            name_lbl.setText(getItemText(item));
        }
        setGraphic(name_lbl);
    }

    public abstract String getItemText(T item);

}
