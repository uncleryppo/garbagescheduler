package de.ithappens.garbagescheduler.view.model;

import com.jfoenix.controls.JFXTextField;
import de.ithappens.garbagescheduler.model.ModelController;
import de.ithappens.garbagescheduler.model.Vault;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Calendar;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import org.apache.commons.lang3.math.NumberUtils;

/**
 * Copyright: 2016 - 2017
 * Organisation: IT-Happens.de
 * @author Christian.Rybotycky
 */
public abstract class I_node extends GridPane {
    
    private final ModelController MC;
    
    public I_node(ModelController mc) {
        MC = mc;
        init();
        buildUi();
    }
    
    public ModelController MC() {
        return MC;
    }
    
    protected Vault getCurrentVault() {
        return vaultExists() ? MC().getCurrentVault() : null;
    }
    
    protected boolean vaultExists() {
        return MC() != null && MC().getCurrentVault() != null;
    }
    
    private void init() {
        int gap = 25;
        int ins = 30;
        setHgap(gap);
        setVgap(gap);
        setPadding(new Insets(ins, ins, ins, ins));
        setPrefSize(1000, 800);
    }
    
    protected abstract void buildUi();
    
    public abstract void bindUi();
    
    public abstract String getTitle();
    
    public static String convertToDateAndTime(Calendar cal) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
        return convertCalendar(sdf, cal);
    }
    
    public static String convertToDate(Calendar cal) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
        return convertCalendar(sdf, cal);
    }
    
    public static String convertCalendar(SimpleDateFormat sdf, Calendar cal) {
        if (cal != null) {
            return sdf.format(cal.getTime());
        } else {
            return null;
        }
    }
    
    public String convert(Integer integer) {
        if (integer != null) {
            return Integer.toString(integer);
        } else {
            return null;
        }
    }
    
    public String convert(Double _double) {
        if (_double != null) {
            return Double.toString(_double);
        } else {
            return null;
        }
    }
    
    public Integer convertToInteger(String string) {
        if (NumberUtils.isNumber(string)) {
            return Integer.parseInt(string);
        } else {
            return null;
        }
    }
    
    public Double convertToDouble(String string) {
        if (NumberUtils.isNumber(string)) {
            return Double.parseDouble(string);
        } else {
            return null;
        }
    }
    
    public JFXTextField notEditableTextField() {
        JFXTextField tf = new JFXTextField();
        tf.setEditable(false);
        return tf;
    }
    
    protected FlowPane buildButtonBar(Button... buttons) {
        FlowPane buttonPane = new FlowPane(buttons);
        buttonPane.setVgap(8);
        buttonPane.setHgap(4);
        return buttonPane;
    }
    
    public Calendar convert(LocalDate ld) {
        if (ld != null) {
            Calendar cal = Calendar.getInstance();
            cal.set(ld.getYear(), ld.getMonthValue() - 1, ld.getDayOfMonth());
            return cal;
        } else {
            return null;
        }
    }
    
    public LocalDate convert(Calendar cal) {
        if (cal != null) {
            return LocalDate.of(cal.get(Calendar.YEAR), 
                    cal.get(Calendar.MONTH) + 1, cal.get(Calendar.DAY_OF_MONTH));
        } else {
            return null;
        }
    }

}
