package de.ithappens.garbagescheduler.view.model;

import com.jfoenix.controls.JFXTextField;
import de.ithappens.garbagescheduler.model.DustBin;
import de.ithappens.garbagescheduler.model.ModelController;
import java.util.List;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.FlowPane;
import lombok.extern.log4j.Log4j2;

/**
 * Copyright: 2016 - 2017
 * Organisation: IT-Happens.de
 * @author Christian.Rybotycky
 */
@Log4j2
public class DustBins_node extends IModel_node<DustBin> {
    
    private JFXTextField uniqueId_txf, sizeInLiter_txf, color_txF;
    private TextArea scopeGarbage_txa, forbiddenGarbage_txa;
    
    public DustBins_node(ModelController mc) {
        super(mc);
    }
    
    @Override
    protected void buildUi() {
        Label uniqueId_lbl = new Label(MC().RB().UNIQUE_ID);
        uniqueId_txf = new JFXTextField();
        uniqueId_txf.textProperty().addListener(
                new DustBinChangeListener() {
            @Override
            public void onChange(String newValue) {
                if (getCurrentModel() != null) {
                    getCurrentModel().setUniqueId(newValue);
                }
                getModelListView().refresh();
            }
        });
        Label sizeInLiter_lbl = new Label(MC().RB().SIZE_IN_LITER);
        sizeInLiter_txf = new JFXTextField();
        sizeInLiter_txf.textProperty().addListener(
                new DustBinChangeListener() {
                    @Override
                    public void onChange(String newValue) {
                        if (getCurrentModel() != null) {
                            getCurrentModel().setSizeInLiter(convertToInteger(newValue));
                        }
                    }
                });
        Label color_lbl = new Label(MC().RB().COLOR);
        color_txF = new JFXTextField();
        color_txF.textProperty().addListener(new DustBinChangeListener() {
            @Override
            public void onChange(String newValue) {
                if (getCurrentModel() != null) {
                    getCurrentModel().setColor(newValue);
                }
            }
        });
        Label scopeGarbage_lbl = new Label(MC().RB().SCOPE_GARBAGE);
        scopeGarbage_txa = new TextArea();
        scopeGarbage_txa.textProperty().addListener(
                new DustBinChangeListener() {
                    @Override
                    public void onChange(String newValue) {
                        if (getCurrentModel() != null) {
                            getCurrentModel().setScopeGarbage(newValue);
                        }
                    }
                });
        Label forbiddenGarbage_lbl = new Label(MC().RB().FORBIDDEN_GARBAGE);
        forbiddenGarbage_txa = new TextArea();
        forbiddenGarbage_txa.textProperty().addListener(new DustBinChangeListener() {
            @Override
            public void onChange(String newValue) {
                if (getCurrentModel() != null) {
                    getCurrentModel().setForbiddenGarbage(newValue);
                }
            }
        });
        //layout
        ColumnConstraints column1 = new ColumnConstraints();
        column1.setPercentWidth(30);
        ColumnConstraints column2 = new ColumnConstraints();
        column2.setPercentWidth(30);
        ColumnConstraints column3 = new ColumnConstraints();
        column3.setPercentWidth(40);
        getColumnConstraints().addAll(column1, column2, column3);
        int row = 0;
        int rowspan = 1;
        int colspan = 1;
     
        FlowPane buttonBar = getButtonBar();
        add(buttonBar, 0, row, 4, rowspan);
        row++;
        add(getModelListView(), 0, row, colspan, 4);
        add(uniqueId_lbl, 1, row);
        add(uniqueId_txf, 2, row);
        row++;
        add(sizeInLiter_lbl, 1, row);
        add(sizeInLiter_txf, 2, row);
        row++;
        add(color_lbl, 1, row);
        add(color_txF, 2, row);
        row++;
        add(scopeGarbage_lbl, 1, row);
        add(scopeGarbage_txa, 2, row);
        row++;
        add(forbiddenGarbage_lbl, 1, row);
        add(forbiddenGarbage_txa, 2, row);
        row++;
    }

    @Override
    public String getTitle() {
        return MC().RB().DUSTBINS;
    }

    @Override
    public void bindToCurrentModel() {
        uniqueId_txf.setText(getCurrentModel() != null ? getCurrentModel().getUniqueId() : null);
        sizeInLiter_txf.setText(getCurrentModel() != null ? convert(getCurrentModel().getSizeInLiter()) : null);
        color_txF.setText(getCurrentModel() != null ? getCurrentModel().getColor(): null);
        scopeGarbage_txa.setText(getCurrentModel() != null ? getCurrentModel().getScopeGarbage(): null);
        forbiddenGarbage_txa.setText(getCurrentModel() != null ? getCurrentModel().getForbiddenGarbage(): null);
    }
    
    @Override
    protected void actionBeforeSaveVault() {
    }
    
    @Override
    protected void actionAfterSaveVault() {
    }
    
    @Override
    protected IModel_listCell<DustBin> getModelCellFactory() {
        return new DustBin_listCell();
    }
    
    @Override
    protected List<DustBin> getModelsFromCurrentVault() {
        return MC().getCurrentVault() != null ? MC().getCurrentVault().getAvailableDustBins(): null;
    }

    @Override
    protected void setModelsToCurrentVault(List<DustBin> models) {
        if(MC().getCurrentVault() != null) {
            MC().getCurrentVault().setAvailableDustBins(models);        } 
    }

    @Override
    protected DustBin createModel() {
        return new DustBin();
    }
    
    abstract class DustBinChangeListener implements ChangeListener<String> {
        
        @Override
        public void changed(ObservableValue<? extends String> ov, String oldValue, String newValue) {
            onChange(newValue);
        }
        
        public abstract void onChange(String newValue);
        
    }
    
}
