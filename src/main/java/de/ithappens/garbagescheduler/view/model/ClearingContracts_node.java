package de.ithappens.garbagescheduler.view.model;

import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import de.ithappens.garbagescheduler.model.ClearingContract;
import de.ithappens.garbagescheduler.model.DustBin;
import de.ithappens.garbagescheduler.model.ModelController;
import java.util.List;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.FlowPane;
import lombok.extern.log4j.Log4j2;

/**
 * opyright: 2016 - 2017
 * rganisation: IT-Happens.de
 * @author Christian.Rybotycky
 */
@Log4j2
public class ClearingContracts_node extends IModel_node<ClearingContract> {
    
    private JFXComboBox<DustBin> dustBin_cbx;
    private DatePicker validFrom_dp, validTo_dp;
    private JFXTextField countIncludedFreeClearings_txf, 
            pricePerClearing_txf, pricePerKilogram_txf;
    
    public ClearingContracts_node(ModelController mc) {
        super(mc);
    }
    
    @Override
    protected void buildUi() {
        Label dustBin_lbl = new Label(MC().RB().DUSTBIN);
        dustBin_cbx = new JFXComboBox<>();
        dustBin_cbx.valueProperty().addListener(new ChangeListener<DustBin>() {

            @Override
            public void changed(ObservableValue<? extends DustBin> ov, DustBin t, DustBin t1) {
                if (currentModelExists()) {
                    getCurrentModel().setDustBin(new DustBin(dustBin_cbx.getValue()));
                }
                getModelListView().refresh();
            }
        });
        Label validFrom_lbl = new Label(MC().RB().VALID_FROM);
        validFrom_dp = new DatePicker();
        validFrom_dp.setOnAction((ActionEvent t) -> {
            if (currentModelExists()) {
                getCurrentModel().setValidFrom(convert(validFrom_dp.getValue()));
            }
            getModelListView().refresh();
        });
        Label validTo_lbl = new Label(MC().RB().VALID_TO);
        validTo_dp = new DatePicker();
        validTo_dp.setOnAction((ActionEvent t) -> {
            if (currentModelExists()) {
                getCurrentModel().setValidTo(convert(validTo_dp.getValue()));
            }
            getModelListView().refresh();
        });
        Label countIncludedFreeClearings_lbl = new Label(MC().RB().COUNT_INCLUDED_FREE_CLEARINGS);
        countIncludedFreeClearings_txf = new JFXTextField();
        countIncludedFreeClearings_txf.textProperty().addListener((ObservableValue<? extends String> ov, String t, String t1) -> {
            if (currentModelExists()) {
                getCurrentModel().setCountIncludedFreeClearings(convertToInteger(t1));
            }
        });
        Label pricePerClearing_lbl = new Label(MC().RB().PRICE_PER_CLEARING);
        pricePerClearing_txf = new JFXTextField();
        pricePerClearing_txf.textProperty().addListener((ObservableValue<? extends String> ov, String t, String t1) -> {
            if (currentModelExists()) {
                getCurrentModel().setPricePerClearing(convertToDouble(t1));
            }
        });
        Label pricePerKilogram_lbl = new Label(MC().RB().PRICE_PER_KILOGRAM);
        pricePerKilogram_txf = new JFXTextField();
        pricePerKilogram_txf.textProperty().addListener((ObservableValue<? extends String> ov, String t, String t1) -> {
            if (currentModelExists()) {
                getCurrentModel().setPricePerKilogram(convertToDouble(t1));
            }
        });
        //layout
        ColumnConstraints column1 = new ColumnConstraints();
        column1.setPercentWidth(30);
        ColumnConstraints column2 = new ColumnConstraints();
        column2.setPercentWidth(30);
        ColumnConstraints column3 = new ColumnConstraints();
        column3.setPercentWidth(40);
        getColumnConstraints().addAll(column1, column2, column3);
        int row = 0;
        int rowspan = 1;
        int colspan = 1;
        
        FlowPane buttonBar = getButtonBar();
        add(buttonBar, 0, row, 4, rowspan);
        row++;
        add(getModelListView(), 0, row, colspan, 4);
        add(dustBin_lbl, 1, row);
        add(dustBin_cbx, 2, row);
        row++;
        add(validFrom_lbl, 1, row);
        add(validFrom_dp, 2, row);
        row++;
        add(validTo_lbl, 1, row);
        add(validTo_dp, 2, row);
        row++;
        add(countIncludedFreeClearings_lbl, 1, row);
        add(countIncludedFreeClearings_txf, 2, row);
        row++;
        add(pricePerClearing_lbl, 1, row);
        add(pricePerClearing_txf, 2, row);
        row++;
        add(pricePerKilogram_lbl, 1, row);
        add(pricePerKilogram_txf, 2, row);
        row++;
    }
    
    @Override
    public String getTitle() {
        return MC().RB().CLEARING_CONTRACTS;
    }
    
    private void bindCombobox(ComboBox<DustBin> combobox, DustBin dustBinToCheck) {
        combobox.getSelectionModel().select(null);
        if (dustBinToCheck != null) {
            ObservableList<DustBin> availableDustBins = combobox.getItems();
            if (availableDustBins != null) {
                boolean itemFound = false;
                for (DustBin availableDustBin : availableDustBins) {
                    if (dustBinToCheck.getUniqueId() != null && dustBinToCheck.getUniqueId().equals(availableDustBin.getUniqueId())) {
                        combobox.getSelectionModel().select(availableDustBin);
                        log.debug(dustBinToCheck.getUniqueId());
                        itemFound = true;
                        break;
                    }
                }
                if (!itemFound) {
                    availableDustBins.add(dustBinToCheck);
                    combobox.getSelectionModel().select(dustBinToCheck);
                }
            }
        }
    }
    
    @Override
    protected void bindToCurrentModel() {
        //reset the UI
        ObservableList<DustBin> dustBins = FXCollections.observableArrayList();
        if (MC().getCurrentVault() != null && MC().getCurrentVault().getAvailableDustBins() != null) {
            for (DustBin availableDustBin : MC().getCurrentVault().getAvailableDustBins()) {
                dustBins.add(new DustBin(availableDustBin));
            }
        }
        dustBin_cbx.setItems(dustBins);
        //bind the UI
        if (getCurrentModel() != null) {
            bindCombobox(dustBin_cbx, getCurrentModel().getDustBin());
            validFrom_dp.setValue(convert(getCurrentModel().getValidFrom()));
            validTo_dp.setValue(convert(getCurrentModel().getValidTo()));
            countIncludedFreeClearings_txf.setText(convert(getCurrentModel().getCountIncludedFreeClearings()));
            pricePerClearing_txf.setText(convert(getCurrentModel().getPricePerClearing()));
            pricePerKilogram_txf.setText(convert(getCurrentModel().getPricePerKilogram()));
        } else {
            dustBin_cbx.getSelectionModel().select(null);
            validFrom_dp.setValue(null);
            validTo_dp.setValue(null);
            countIncludedFreeClearings_txf.setText(null);
            pricePerClearing_txf.setText(null);
            pricePerKilogram_txf.setText(null);
        }
    }

    @Override
    protected IModel_listCell<ClearingContract> getModelCellFactory() {
        return new ClearingContract_listCell();
    }

    @Override
    protected List<ClearingContract> getModelsFromCurrentVault() {
        return vaultExists() ? getCurrentVault().getClearingContracts() : null;
    }

    @Override
    protected void setModelsToCurrentVault(List<ClearingContract> models) {
        if (vaultExists()) {
            getCurrentVault().setClearingContracts(models);
        }
    }

    @Override
    protected ClearingContract createModel() {
        return new ClearingContract();
    }

    @Override
    protected void actionBeforeSaveVault() {
    }

    @Override
    protected void actionAfterSaveVault() {
    }

}
