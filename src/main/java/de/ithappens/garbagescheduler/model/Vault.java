package de.ithappens.garbagescheduler.model;

import java.util.Calendar;
import java.util.List;

/**
 * Copyright: 2016 - 2017
 * Organisation: IT-Happens.de
 * @author Christian.Rybotycky
 */
public class Vault {

    private List<ClearingContract> clearingContracts;
    private List<ClearingEvent> clearingEvents;
    private List<DustBin> availableDustBins;
    private Calendar creationDate, changeDate;
    private String creationUser, creationSystem, changeUser, changeSystem;

    public List<ClearingContract> getClearingContracts() {
        return clearingContracts;
    }

    public void setClearingContracts(List<ClearingContract> clearingContracts) {
        this.clearingContracts = clearingContracts;
    }
    
    public Calendar getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Calendar creationDate) {
        this.creationDate = creationDate;
    }

    public Calendar getChangeDate() {
        return changeDate;
    }

    public void setChangeDate(Calendar changeDate) {
        this.changeDate = changeDate;
    }

    public String getCreationUser() {
        return creationUser;
    }

    public void setCreationUser(String creationUser) {
        this.creationUser = creationUser;
    }

    public String getCreationSystem() {
        return creationSystem;
    }

    public void setCreationSystem(String creationSystem) {
        this.creationSystem = creationSystem;
    }

    public String getChangeUser() {
        return changeUser;
    }

    public void setChangeUser(String changeUser) {
        this.changeUser = changeUser;
    }

    public String getChangeSystem() {
        return changeSystem;
    }

    public void setChangeSystem(String changeSystem) {
        this.changeSystem = changeSystem;
    }

    public List<ClearingEvent> getClearingEvents() {
        return clearingEvents;
    }

    public void setClearingEvents(List<ClearingEvent> clearingEvents) {
        this.clearingEvents = clearingEvents;
    }

    public List<DustBin> getAvailableDustBins() {
        return availableDustBins;
    }

    public void setAvailableDustBins(List<DustBin> availableDustBins) {
        this.availableDustBins = availableDustBins;
    }
    
}
