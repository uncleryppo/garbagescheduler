package de.ithappens.garbagescheduler.model;

import java.util.Calendar;
import lombok.Getter;
import lombok.Setter;

/**
 * Copyright: 2016 - 2017
 * Organisation: IT-Happens.de
 * @author Christian.Rybotycky
 */
public class ClearingContract implements I_model<ClearingContract> {
    
    @Getter @Setter private Calendar validFrom, validTo;
    @Getter @Setter private DustBin dustBin;
    @Getter @Setter private Integer countIncludedFreeClearings;
    @Getter @Setter private Double pricePerClearing, pricePerKilogram;

    @Override
    public int compareTo(ClearingContract o) {
        if (o == null || o.getValidFrom()== null) {
            return -1;
        }
        if (getValidFrom() == null) {
            return 1;
        }
        return o.getValidFrom().compareTo(getValidFrom());
    }

}
