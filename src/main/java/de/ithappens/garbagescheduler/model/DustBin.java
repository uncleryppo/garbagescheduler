package de.ithappens.garbagescheduler.model;

import lombok.Getter;
import lombok.Setter;

/**
 * Copyright: 2016 - 2017
 * Organisation: IT-Happens.de
 * @author Christian.Rybotycky
 */
public class DustBin implements I_model<DustBin>{
    
    @Getter @Setter private String uniqueId;
    @Getter @Setter private Integer sizeInLiter;
    @Getter @Setter private String color, scopeGarbage, forbiddenGarbage;
    
    public DustBin() {
    }
    
    public DustBin(DustBin source) {
        if (source != null) {
            uniqueId = source.getUniqueId();
            sizeInLiter = source.getSizeInLiter();
            color = source.getColor();
            scopeGarbage = source.getScopeGarbage();
            forbiddenGarbage = source.getForbiddenGarbage();
        }
    }
    
    @Override
    public String toString() {
        return uniqueId + ": " + color;
    }

    @Override
    public int compareTo(DustBin o) {
        if (o == null || o.getColor() == null) {
            return -1;
        }
        if (getColor() == null) {
            return 1;
        }
        return o.getColor().compareTo(getColor());
    }

}
