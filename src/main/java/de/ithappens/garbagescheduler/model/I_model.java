package de.ithappens.garbagescheduler.model;

/**
 * Copyright: 2016
 * Organisation: IT-Happens.de
 * @author Christian.Rybotycky
 * @param <E>
 */
public interface I_model<E extends I_model> extends Comparable<E> {

}
