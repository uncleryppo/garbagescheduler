package de.ithappens.garbagescheduler.model;

import java.util.Calendar;
import lombok.Getter;
import lombok.Setter;
import org.codehaus.jackson.annotate.JsonIgnore;

/**
 * Copyright: 2016 - 2017
 * Organisation: IT-Happens.de
 * @author Christian.Rybotycky
 */
public class ClearingEvent implements I_model<ClearingEvent>{
    
    @Getter @Setter private DustBin[] clearedDustBins, plannedDustBinsToClear;
    @Getter @Setter private Calendar dateOfPlannedClearing, dateOfExecutedClearing;

    @JsonIgnore
    public Calendar getDate() {
        return dateOfExecutedClearing != null ? dateOfExecutedClearing : dateOfPlannedClearing;
    }

    @Override
    public int compareTo(ClearingEvent o) {
        if (o == null || o.getDate() == null) {
            return -1;
        }
        if (getDate() == null) {
            return 1;
        }
        return o.getDate().compareTo(getDate());
    }

}
