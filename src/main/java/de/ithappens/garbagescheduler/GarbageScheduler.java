package de.ithappens.garbagescheduler;

import com.jpro.webapi.JProApplication;
import de.ithappens.garbagescheduler.model.ModelController;
import de.ithappens.garbagescheduler.view.ApplicationFrame;
import de.ithappens.garbagescheduler.view.IconDictionary;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.stage.Stage;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;

/**
 * Copyright: 2016 - 2017
 * Organisation: IT-Happens.de
 * @author Christian.Rybotycky
 */
@Log4j2
public class GarbageScheduler extends JProApplication {

    private static boolean undecorated = true;

    private static final String appId = "Garbage Scheduler";
    private static String[] args;

    /**
     * @param _args the command line arguments
     */
    public static void main(String[] _args) {
        args = _args;
        if (isOSX()) {
            System.setProperty("apple.awt.graphics.EnableQ2DX", "true");
            System.setProperty("com.apple.mrj.application.apple.menu.about.name", appId);
            System.setProperty("apple.laf.useScreenMenuBar", "true");
            com.apple.eawt.Application.getApplication().setDockIconBadge("Y3");
            com.apple.eawt.Application.getApplication().setDockIconImage(IconDictionary.APPLICATION__IMAGE_ICON.getImage());
        }
        readArgs();
        launch(args);
    }

    public static void readArgs() {
        if (args != null) {
            for (String arg : args) {
                undecorated = !StringUtils.contains(arg, "-decorated");
            }
        }
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        log.debug("Starting modelController");
        ModelController mc = new ModelController();
        log.debug("Starting application " + mc.RB().APPLICATION_NAME + " in version " + mc.RB().APPLICATION_VERSION);
        ApplicationFrame applicationFrame = new ApplicationFrame(primaryStage, undecorated, mc);
        applicationFrame.setVisible(true);
        applicationFrame.loadLastUsedVaultFile();
    }

    public static boolean isOSX() {
        String osName = System.getProperty("os.name").toLowerCase();
        return osName.contains("mac");
    }

}
